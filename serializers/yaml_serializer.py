#!/usr/bin/python3
# -*- coding: utf-8 -*-

import yaml
from json_serializer import JsonSerializer

class YamlSerializer(JsonSerializer):
    def to_str(self):
        return yaml.dump(self._current_object)