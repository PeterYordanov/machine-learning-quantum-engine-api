#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import Flask
from flask import request
from core.prediction_manager import PredictionManager
from core.model_factory import ModelType
from utils.logger import get_logger
from pymongo import MongoClient
from bson.objectid import ObjectId
from core.quantum_prediction_manager import QuantumPredictionManager
from utils.mini_json_encoder import MiniJSONEncoder
from flask_restplus import Api, Resource
import json
import configparser
import os
import sys

# Setup
app = Flask(__name__)
api = Api(app=app)
index_name_space = api.namespace('Default', 'Contains index')
main_name_space = api.namespace('Quantum Computing and Machine Learning', description='Core engine for performing quantum machine learning computations and machine learning computations, along with generating graphs to describe the source dataset.')

app.json_encoder = MiniJSONEncoder
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
config = configparser.ConfigParser()

if os.environ.get('ENVIRONMENT') is None:
    os.environ['ENVIRONMENT'] = 'Development'

get_logger().info('Environment: ' + os.environ.get('ENVIRONMENT'))
config.read('config.' + os.environ.get('ENVIRONMENT') + '.ini')

TRAIN_DATA_PATH = 'data/train.csv'

quantum_prediction_manager = QuantumPredictionManager(TRAIN_DATA_PATH)
get_logger().info('{0} set up successfully'.format("QSVM"))
all_prediction_managers = []


@main_name_space.route('/')
class MainHttpHandler(Resource):

    @api.doc(responses={ 200: 'OK', 500: 'Service might not be running' })
    @app.route('/save_graphs_s3')
    def get():
        PredictionManager(TRAIN_DATA_PATH).save_graphs_s3()
        get_logger().info('Graphs saved to S3')

        return {
            'Status': 'Saved'
        }

    @api.doc(responses={ 200: 'OK', 400: 'Invalid Argument', 500: 'Service might not be running' }, 
			 params={ 'mongo_id': 'ObjectId of the entry in a Mongo Database' })
    @app.route('/model/predict_all', methods=['GET', 'POST'])
    def post():
        mongo_object_id = request.args.get('mongo_id')

        client = MongoClient(config['mongodb']['connection_string'])
        db = client.customers_db
        collection = db.customers_collection
        mongo_object = [i for i in collection.find({"_id": ObjectId(mongo_object_id)})]
        print(mongo_object)

        revolvingutilizationofunsecuredlines = float(mongo_object[0]['RevolvingUtilizationOfUnsecuredLines'])
        age = mongo_object[0]['Age']
        numberoftime3059dayspastduenotworse = mongo_object[0]['NumberOfTime3059DaysPastDueNotWorse']
        debtratio = float(mongo_object[0]['DebtRatio'])
        monthlyincome = float(mongo_object[0]['MonthlyIncome'])
        numberofopencreditlinesandloans = mongo_object[0]['NumerOfOpenCreditLinesAndLoans']
        numberoftimes90dayslate = mongo_object[0]['NumberOfTimes90DaysLate']
        numberrealestateloansorlines = mongo_object[0]['NumberOfRealEstateLoansOrLines']
        numberoftime6089dayspastduenotworse = mongo_object[0]['NumberOfTime6089DaysPastDueNotWorse']
        numberofdependents = mongo_object[0]['NumberOfDependents']
        
        res = []
        input_data = [[revolvingutilizationofunsecuredlines, 
                        age, 
                        numberoftime3059dayspastduenotworse, 
                        debtratio, 
                        monthlyincome,
                        numberofopencreditlinesandloans,
                        numberoftimes90dayslate,
                        numberrealestateloansorlines,
                        numberoftime6089dayspastduenotworse,
                        numberofdependents]]

        qsvm_prediction = quantum_prediction_manager.predict(input_data)
        qsvm_accuracy = quantum_prediction_manager.accuracy()
        res.append({
            'ModelType': 'QSVM',
            'Predicted': str(qsvm_prediction),
            'AccuracyScore': str(qsvm_accuracy)
        })
        get_logger().info('Prediction Made: [{0}] [{1}] {2} {3}'.format(str(qsvm_prediction), str(''), str(''), str(qsvm_prediction)))

        for pred_manager in all_prediction_managers:
            y_prediction, y_prediction_probability = pred_manager.predict(input_data)        
            roc_auc = pred_manager.roc_auc_score()
            accuracy = pred_manager.accuracy_score()

            get_logger().info('Prediction Made: {0} {1} {2} {3}'.format(str(y_prediction), str(y_prediction_probability), str(roc_auc), str(accuracy)))
            
            res.append({
                'ModelType': type(pred_manager.model).__name__,
                'Predicted': str(y_prediction[0]),
                'PredictedProbability': str(y_prediction_probability[0]),
                'RocAucScore': str(roc_auc),
                'AccuracyScore': str(accuracy)
            })

        return json.dumps(res, indent=4)


if __name__ == '__main__':

    if sys.version_info.major != 3:
        print('This Python is only compatible with Python 3, but you are running '
            'Python {}. The installation will likely fail.'.format(sys.version_info.major))

    prediction_manager_knn = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_knn.set_model(ModelType.KNN)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_knn.model).__name__)))
    all_prediction_managers.append(prediction_manager_knn)

    prediction_manager_svm = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_svm.set_model(ModelType.SVM)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_svm.model).__name__)))
    all_prediction_managers.append(prediction_manager_svm)

    prediction_manager_mlp = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_mlp.set_model(ModelType.MLP)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_mlp.model).__name__)))
    all_prediction_managers.append(prediction_manager_mlp)

    prediction_manager_random_forests = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_random_forests.set_model(ModelType.RandomForests)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_random_forests.model).__name__)))
    all_prediction_managers.append(prediction_manager_random_forests)

    prediction_manager_logistic_regression = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_logistic_regression.set_model(ModelType.LogisticRegression)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_logistic_regression.model).__name__)))
    all_prediction_managers.append(prediction_manager_logistic_regression)

    prediction_manager_gaussian_nb = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_gaussian_nb.set_model(ModelType.GaussianNB)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_gaussian_nb.model).__name__)))
    all_prediction_managers.append(prediction_manager_gaussian_nb)

    prediction_manager_ada_boost = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_ada_boost.set_model(ModelType.AdaBoost)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_ada_boost.model).__name__)))
    all_prediction_managers.append(prediction_manager_ada_boost)

    prediction_manager_gradient_boost = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_gradient_boost.set_model(ModelType.GradientBoost)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_gradient_boost.model).__name__)))
    all_prediction_managers.append(prediction_manager_gradient_boost)

    prediction_manager_decision_tree = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_decision_tree.set_model(ModelType.DecisionTree)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_decision_tree.model).__name__)))
    all_prediction_managers.append(prediction_manager_decision_tree)

    prediction_manager_extra_trees = PredictionManager(TRAIN_DATA_PATH)
    prediction_manager_extra_trees.set_model(ModelType.ExtraTrees)
    get_logger().info('{0} set up successfully'.format(str(type(prediction_manager_extra_trees.model).__name__)))
    all_prediction_managers.append(prediction_manager_extra_trees)

    app.run(
        #host='0.0.0.0',
        port=8083,
        debug=True,
        threaded=False
    )
