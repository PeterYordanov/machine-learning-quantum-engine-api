#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

from qiskit.aqua.components.feature_maps import SecondOrderExpansion
from qiskit.ml.datasets import wine, sample_ad_hoc_data, ad_hoc_data
from qiskit import BasicAer
from qiskit.circuit.library import ZZFeatureMap
from qiskit.aqua import QuantumInstance, aqua_globals
from qiskit.aqua.algorithms import QSVM
from qiskit.aqua.components.multiclass_extensions import AllPairs
from qiskit.aqua.utils.dataset_helper import get_feature_dimension
from qiskit.test.providers import provider
from sklearn.model_selection import train_test_split
from .data_loader import DataLoader


class QuantumPredictionManager:
    def __init__(self, file_path):
        self.data_loader = DataLoader()
        self.X_train, self.X_test, self.y_train, self.y_test = self.data_loader.import_and_reprocess(file_path, True)

        self.training_dataset = {'A': self.X_train,
                                 'B': self.y_train}

        self.test_dataset = {'A': self.X_test,
                             'B': self.y_test}

        seed_in = 10599
        aqua_globals.random_seed = seed_in
        shots = 192
        num_qubits = 10

        total_arr = np.concatenate((self.test_dataset['A'], self.test_dataset['B']))
        backend = BasicAer.get_backend('qasm_simulator')
        feature_map = ZZFeatureMap(feature_dimension=get_feature_dimension(self.training_dataset), reps=2, entanglement='linear')
        self.svm = QSVM(feature_map, self.training_dataset, self.test_dataset) # Creation of QSVM
        self.quantum_instance = QuantumInstance(backend, shots=shots, skip_qobj_validation=False)
        self.result = self.svm.run(self.quantum_instance)
        self.testing_accuracy = self.result['testing_accuracy']

    def predict(self, input):
        prediction = self.svm.predict(np.array(input), self.quantum_instance) # Predict using unlabelled data
        return prediction[0]

    def accuracy(self):
        return self.testing_accuracy
    
    def __copy__(self):
        return PredictionManager(self.name)
    
    def __deepcopy__(self, memo):
        return PredictionManager(copy.deepcopy(self.name, memo))