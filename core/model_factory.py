#!/usr/bin/python3
# -*- coding: utf-8 -*-

from .model_save import save_model
from utils.enumeration import Enum

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn import tree
from sklearn import svm
from sklearn.ensemble import ExtraTreesClassifier

ModelType = Enum('LogisticRegression',
                 'AdaBoost',
                 'KNN',
                 'GradientBoost',
                 'RandomForests',
                 'SVM',
                 'MLP',
                 'GaussianNB',
                 'DecisionTree',
                 'ExtraTrees')

def create_model(model_type, should_save_model = False):

    switcher = {
        0 : LogisticRegression(penalty='l1', 
                               dual=False, 
                               tol=0.0001, 
                               C=1.0, 
                               fit_intercept=True,
                               intercept_scaling=1, 
                               class_weight=None, 
                               random_state=None, 
                               solver='liblinear', 
                               max_iter=100,
                               multi_class='ovr', 
                               verbose=0),

        1 : AdaBoostClassifier(base_estimator=None, 
                               n_estimators=200, 
                               learning_rate=1.0),

        2 : KNeighborsClassifier(n_neighbors=5, 
                                 weights='uniform', 
                                 algorithm='auto', 
                                 leaf_size=30, 
                                 p=2,
                                 metric='minkowski', 
                                 metric_params=None),

        3 : GradientBoostingClassifier(loss='deviance', 
                                       learning_rate=0.1, 
                                       n_estimators=200, 
                                       subsample=1.0,
                                       min_samples_split=2, 
                                       min_samples_leaf=1, 
                                       min_weight_fraction_leaf=0.0, 
                                       max_depth=3,
                                       init=None, 
                                       random_state=None, 
                                       max_features=None, 
                                       verbose=0),

        4 : RandomForestClassifier(n_estimators=10, 
                                   criterion='gini', 
                                   max_depth=None, 
                                   min_samples_split=2,
                                   min_samples_leaf=1, 
                                   min_weight_fraction_leaf=0.0, 
                                   max_features='auto',
                                   max_leaf_nodes=None, 
                                   bootstrap=True, 
                                   oob_score=False, 
                                   n_jobs=1, 
                                   random_state=None, 
                                   verbose=0),

        5 : svm.SVC(C=2,
                    cache_size=7000,
                    tol=1,
                    class_weight={0:.1,1:.9}, 
                    probability=True),

        6 : MLPClassifier(activation="identity", tol=0.1),

        7 : GaussianNB(priors=[0.07,0.93]),

        8 : tree.DecisionTreeClassifier(class_weight='balanced',
                                        min_impurity_decrease=1e-05,
                                        max_depth=6),

        9 : ExtraTreesClassifier(4,random_state=100)
    }

    model = switcher.get(model_type, "nothing")

    if should_save_model:
        save_model(model, type(model).__name__)

    return model
