#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import copy

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection  import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.metrics import accuracy_score
from sklearn.metrics import roc_auc_score

from .model_save import save_model
from .model_factory import create_model
from .model_factory import ModelType

from .data_loader import DataLoader


class PredictionManager:
    def __init__(self, file_path):
        self.data_loader = DataLoader()
        self.X_train, self.X_test, self.y_train, self.y_test = self.data_loader.import_and_reprocess(file_path)

    def set_model(self, model_type, save_model=False):
        self.model = create_model(model_type, save_model)
        self.model.fit(self.X_train, self.y_train)
        self.y_prediction = self.model.predict(np.array(self.X_test))
        self.accuracy_score_metric = accuracy_score(self.y_test, self.y_prediction, normalize=True)
        test_labels = self.model.predict_proba(np.array(self.X_test.values))[:,1]
        self.roc_auc_score_metric = roc_auc_score(self.y_test, test_labels, average='macro', sample_weight=None)

    def predict(self, input_data):
        y_prediction_probability = self.model.predict_proba(np.array(input_data))[:,1]

        y_prediction = self.model.predict(np.array(input_data))
        
        return y_prediction, y_prediction_probability

    def roc_auc_score(self):
        return self.roc_auc_score_metric

    def accuracy_score(self):
        return self.accuracy_score_metric

    def save_graphs_s3(self):
        self.data_loader.save_graphs_s3()

    def __copy__(self):
        return PredictionManager(self.name)
    
    def __deepcopy__(self, memo):
        return PredictionManager(copy.deepcopy(self.name, memo))