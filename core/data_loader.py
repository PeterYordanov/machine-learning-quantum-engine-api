#!/usr/bin/python3
# -*- coding: utf-8 -*-

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, roc_curve, auc
from sklearn import datasets
from random import randrange, uniform

import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import sys
import boto3
import io


class DataLoader:
    def import_and_reprocess(self, file_path, transform = False):
        train = pd.read_csv(file_path).drop('Unnamed: 0', axis = 1)
        self.training_data = train.copy()

        cleancolumn = []
        for i in range(len(train.columns)):
            cleancolumn.append(train.columns[i].replace('-', '').lower())

        train.columns = cleancolumn.copy()

        # Data needed for visualization
        self.total_len = len(train['seriousdlqin2yrs'])
        percentage_labels = (train['seriousdlqin2yrs'].value_counts() / self.total_len) * 100
        
        self.count_plot = train.seriousdlqin2yrs

        train_mean = train.fillna((train.mean()))

        train_median = train.fillna((train.median()))

        train.fillna((train.median()), inplace=True)

        # Get the correlation of the training dataset
        correlation=train[train.columns[1:]].corr()

        X = train.drop('seriousdlqin2yrs',axis=1)
        y = train.seriousdlqin2yrs

        features_label = train.columns[1:]

        self.X = train.drop('seriousdlqin2yrs', axis=1)
        self.y = train.seriousdlqin2yrs

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, test_size = 0.20, random_state = 1)

        if transform:
            self.X_train = []
            self.X_test = []
            self.y_train = []
            self.y_test = []
            for i in range(0, 20):
                self.X_train.append([uniform(0.001, 1.1), randrange(30, 99), randrange(0, 2), uniform(0.5, 5000),
                                    uniform(1500, 17000), randrange(0, 10), randrange(0, 1), randrange(0, 2), randrange(0, 1),
                                    randrange(0, 1)])
                self.y_train.append([uniform(0.001, 1.1), randrange(18, 30), randrange(2, 5), uniform(0.1, 0.5),
                                uniform(500, 1500), randrange(10, 40), randrange(0, 1), randrange(2, 10), randrange(1, 2),
                                randrange(2, 5)])

            for i in range(0, 2):
                self.X_test.append([uniform(0.001, 1.1), randrange(30, 99), randrange(0, 2), uniform(0.5, 5000),
                                    uniform(1500, 17000), randrange(0, 10), randrange(0, 1), randrange(0, 2), randrange(0, 1),
                                    randrange(0, 1)])
                self.y_test.append([uniform(0.001, 1.1), randrange(18, 30), randrange(2, 5), uniform(0.1, 0.5),
                                uniform(500, 1500), randrange(10, 40), randrange(0, 1), randrange(2, 10), randrange(1, 2),
                                randrange(2, 5)])

            '''
            self.X_train = np.asarray(self.X_train)
            self.X_test = np.asarray(self.X_test)
            self.y_train = np.asarray(self.y_train)
            self.y_test = np.asarray(self.y_test)
            '''
        return self.X_train, self.X_test, self.y_train, self.y_test
        
    def plot_confusion_matrix(cm, classes,
                              normalize=False,
                              title='Confusion matrix',
                              cmap=plt.cm.Blues):
        if normalize:
            cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')

        print(cm)

        plt.imshow(cm, interpolation='nearest', cmap=cmap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, format(cm[i, j], fmt),
                    horizontalalignment="center",
                    color="white" if cm[i, j] > thresh else "black")

        plt.ylabel('True label')
        plt.xlabel('Predicted label')
        plt.tight_layout()

    def save_graphs_s3(self):
        BUCKET_NAME = 'credit-risk-analysis'
        s3 = boto3.resource(
                    's3',
                    aws_access_key_id='AKIAIRTN5WSMDKWHJ5OA',
                    aws_secret_access_key='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',
                    region_name='us-west-1')
        bucket = s3.Bucket(BUCKET_NAME)

        #
        # Data Distribution
        #
        sns.set()
        sns.countplot(self.count_plot).set_title('Data Distribution')
        ax = plt.gca()
        for p in ax.patches:
            height = p.get_height()
            ax.text(p.get_x() + p.get_width() / 2.,
                    height + 2,
                    '{:.2f}%'.format(100 * (height / self.total_len)),
                    fontsize=14, ha='center', va='bottom')
        sns.set(font_scale=1.5)
        ax.set_xlabel("Labels for seriousdlqin2yrs attribute")
        ax.set_ylabel("Numbers of records")

        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)

        bucket.put_object(ACL='public-read', Body=img_data, ContentType='image/png', Key='Data_Distribution/data_distribution.png')
        plt.close()

        #
        # Missing Records
        #
        x = self.training_data.columns
        y = self.training_data.isnull().sum()
        sns.set()
        sns.barplot(x,y)
        ax = plt.gca()
        for p in ax.patches:
            height = p.get_height()
            ax.text(p.get_x() + p.get_width() / 2.,
                    height + 2,
                    int(height),
                    fontsize=14, ha='center', va='bottom')
        sns.set(font_scale=1.5)
        ax.set_xlabel("Data Attributes")
        ax.set_ylabel("count of missing records for each attribute")
        plt.xticks(rotation=90)

        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)

        bucket.put_object(ACL='public-read', Body=img_data, ContentType='image/png', Key='Missing_Records/missing_records.png')
        plt.close()
        
        #
        # Heatmap
        #
        sns.set()
        sns.set(font_scale=1.25)
        sns.heatmap(self.training_data[self.training_data.columns[1:]].corr(),annot=True,fmt=".1f")

        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)

        bucket.put_object(ACL='public-read', Body=img_data, ContentType='image/png', Key='Heatmap/heatmap.png')
        plt.close()

        #
        # Feature Importances
        #
        importances = RandomForestClassifier(n_estimators = 200, 
                                             criterion = 'entropy', 
                                             random_state = 0).fit(self.X_train, self.y_train).feature_importances_
        indices = np.argsort(importances)[::-1]
        features_label = self.training_data.columns[1:]

        plt.title('Feature Importances')
        plt.bar(range(self.X.shape[1]), importances[indices], color="green", align="center")
        plt.xticks(range(self.X.shape[1]), features_label, rotation=90)
        plt.xlim([-1, self.X.shape[1]])

        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)

        bucket.put_object(ACL='public-read', Body=img_data, ContentType='image/png', Key='Feature_Importance/feature_importances.png')
        plt.close()
        
        #
        # Outlier detection
        #
        #plt.figure(figsize=(20,15))
        ax = plt.subplot(211)
        #ax.set_ylim(0,20)
        plt.plot(self.training_data.RevolvingUtilizationOfUnsecuredLines, 'bo',self.training_data.RevolvingUtilizationOfUnsecuredLines, 'k')
        ruoelLt2=len(self.training_data[self.training_data.RevolvingUtilizationOfUnsecuredLines < 2])
        ruoelACt=len(self.training_data.RevolvingUtilizationOfUnsecuredLines)
        
        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)

        bucket.put_object(ACL='public-read', Body=img_data, ContentType='image/png', Key='Outlier_Detection/outlier_detection.png')
        plt.close()
        
        #
        # Age
        #
        plt.figure(2)
        sns.set_color_codes()
        sns.distplot(self.training_data.age, color="y")

        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)

        bucket.put_object(ACL='public-read', Body=img_data, ContentType='image/png', Key='Age_Distribution/age.png')
        plt.close()