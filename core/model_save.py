#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pickle

def save_model(model, model_name):
    with open(model_name + '.pkl', 'wb') as f:
        pickle.dump(model, f)