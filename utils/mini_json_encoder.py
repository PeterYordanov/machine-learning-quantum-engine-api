#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask.json import JSONEncoder

class MiniJSONEncoder(JSONEncoder):
    item_separator = ','
    key_separator = ':'