#!/usr/bin/python3
# -*- coding: utf-8 -*-

class Singleton(type):
    _instances = {}
    def __new__(class_, *args, **kwargs):
        if class_ not in class_._instances:
            class_._instances[class_] = super(Singleton, class_).__new__(class_, *args, **kwargs)
        # else:
        #    cls._instances[cls].__init__(*args, **kwargs)
        
        return class_._instances[class_]