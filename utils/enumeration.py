#!/usr/bin/python3
# -*- coding: utf-8 -*-

def Enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)