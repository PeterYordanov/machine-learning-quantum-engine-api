#!/bin/bash

echo "* Setup and install Python..."
pip3 install pipreqs
pip3 install -r requirements.txt

echo "* Setup and install MongoDB..."
apt-get install -y gnupg
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list
apt-get update -y
apt-get install -y mongodb-org

echo "* Start MongoDB..."
systemctl start mongod
systemctl daemon-reload
systemctl status mongod
systemctl enable mongod

echo "* Install Flask..."
apt-get install -y python3-flask

