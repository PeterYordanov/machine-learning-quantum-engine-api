#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

requires = [
    'flask',
    'flask-sqlalchemy',
    'psycopg2',
]

setup(
    name='ml_engine',
    version='0.1',
    description='Machine Learning Engine',
    author='Peter Yordanov',
    author_email='peter.yordanov0@gmail.com',
    keywords='web flask machine learning ml api',
    packages=find_packages(),
    include_package_data=True,
    install_requires=requires
)