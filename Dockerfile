FROM python:3.8 AS builder

FROM python:3.8-slim
WORKDIR /code

#COPY --from=builder /root/.local/bin /root/.local
COPY . .
RUN pip install -r requirements.txt

#ENV PATH=/root/.local:$PATH

CMD [ "python", "./main.py" ]